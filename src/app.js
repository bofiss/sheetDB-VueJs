
import Vue from 'vue'
import axios from 'axios'
var SnazzyInfoWindow = require('snazzy-info-window')
var c3 = require('c3')

new Vue({
    el: "#app",

    data: {
        infos: [],
        mapName: 'sheetmap',
        map: null,
        center:null,
        bounds: null,
        markers: [],
        countries: [],
        options: null,
        country: 'United Kingdom',
        customIcon: null,
        cards: [],
        prices_cards: []
    },
    methods: {

       loadCountryData(country) {
       
            this.markers = []
            this.bounds = new google.maps.LatLngBounds()
           
            this.infos.filter(data => data.Country === country).map(data => {
                
                let position =  this.center =  new google.maps.LatLng(data.Latitude,data.Longitude)
                const marker = new google.maps.Marker({ 
                        position,
                        map: this.map,
                        title: data.City,
                        icon: this.customIcon
                })
                let info = new SnazzyInfoWindow({
                    marker: marker,
                    content: `<h1 class="title has-text-centered">${data.Name}</h1> 
                             <p>
                                <span>Card: ${data.Payment_Type}</span><br>
                                <span>Price: <strong>${data.Price}</strong></span>
                             </p> 
                              <hr> 
                              <em>${data.City} - ${data.State}</em>`,
                    closeOnMapClick: false
                })
                this.markers.push(marker)
                this.map.fitBounds(this.bounds.extend(position))
            })
            this.loadTransactionCard(country)
       },
       
       loadTransactionCard(country) {
          this.prices_cards=[]
          this.cards.map((card) => {
              console.log(card)
            this.prices_cards.push(this.infos.filter(data => data.Payment_Type === card && data.Country === country)
             .map(data => data.Price)
             .reduce((accumulator, currentValue) => parseInt(accumulator) + parseInt(currentValue), 0))
          })
         
          let chart = c3.generate({
            bindto: '#chart',
            data: {
              columns: [
                ['data1', ...this.prices_cards]
              ],
              types: {
                data1: 'bar',
              }
            },
            axis: {
                x: {
                    type: 'category',
                    categories: this.cards
                }
               
            }
        })

       }

       

    },

    mounted() {

        let MAP_PIN = 'M0-48c-9.8 0-17.7 7.8-17.7 17.4 0 15.5 17.7 30.6 17.7 30.6s17.7-15.4 17.7-30.6c0-9.6-7.9-17.4-17.7-17.4z'
        let SQUARE_PIN = 'M22-48h-44v43h16l6 5 6-5h16z'
        var mapStyle = [{'featureType': 'administrative', 'elementType': 'labels.text.fill', 'stylers': [{'color': '#444444'}]}, {'featureType': 'landscape', 'elementType': 'all', 'stylers': [{'color': '#f2f2f2'}]}, {'featureType': 'poi', 'elementType': 'all', 'stylers': [{'visibility': 'off'}]}, {'featureType': 'road', 'elementType': 'all', 'stylers': [{'saturation': -100}, {'lightness': 45}]}, {'featureType': 'road.highway', 'elementType': 'all', 'stylers': [{'visibility': 'simplified'}]}, {'featureType': 'road.arterial', 'elementType': 'labels.icon', 'stylers': [{'visibility': 'off'}]}, {'featureType': 'transit', 'elementType': 'all', 'stylers': [{'visibility': 'off'}]}, {'featureType': 'water', 'elementType': 'all', 'stylers': [{'color': '#4f595d'}, {'visibility': 'on'}]}];
       
        this.customIcon = {
            path: MAP_PIN,
            fillColor: '#00CCBB',
            fillOpacity: 1,
            scale: 0.8,
            strokeWeight: 0
        }

        this.bounds = new google.maps.LatLngBounds()
        const element = document.getElementById(this.mapName)
        this.center = new google.maps.LatLng(51.5,-1.1166667)
        this.options = {
            zoom: 12,
            center: this.center,
            draggable: true,
            styles: mapStyle
        }
        this.map = new google.maps.Map(element, this.options);
        axios.get('https://sheetdb.io/api/v1/5aa65dabc02ac').then((response) => {
           
            this.countries = [ ...new Set(response.data.map(data => data.Country))]
            this.cards =  [...new Set(response.data.map(data => data.Payment_Type))]
            console.log(this.cards)
            response.data.filter(data => data.Country === this.country).map(data => {
                let position = new google.maps.LatLng(data.Latitude,data.Longitude)
                
                const marker = new google.maps.Marker({ 
                     position,
                     map: this.map,
                     title: data.City,
                     icon: this.customIcon
                    
                })
               let info = new SnazzyInfoWindow({
                    marker: marker,
                    content: `<h1>${data.Name}</h1> 
                             <p>
                                <span>Card: ${data.Payment_Type}</span><br>
                                <span>Price: <strong>${data.Price}</strong></span>
                             </p> 
                              <hr> 
                              <em>${data.City} - ${data.State}</em>`,
                    closeOnMapClick: false
                })
                this.markers.push(marker)
                this.map.fitBounds(this.bounds.extend(position))

            })

            this.infos = response.data
            
       })
       

    }

    
})
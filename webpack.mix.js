const mix = require('laravel-mix')

mix.js('src/app.js', 'dist')
mix.js('src/map-icons.min.js', 'dist')
mix.copyDirectory('src/fonts', 'dist/fonts');
mix.browserSync({
    proxy: 'vue-sheetdb.test',
    host: 'vue-sheetdb.test',
    notify: false,
    files: ["*.html", "./dist/js/*.js", "./dist/css/*.css"]
})

